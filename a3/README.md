> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Assignment 3 Requirements:

*Three Parts:*

1. Create a mySQL database with three tables
2. Enter 10 inserts in each table
3. Chapter Questions (Ch 7-8)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Link to the following files:
	* a3.mwb
	* a3.sql

[Workbench file](a3.mwb)

[SQL Statements](a3.sql)


#### Assignment Screenshots:

*Screenshot of query results:

![ERD](img/database.png)



