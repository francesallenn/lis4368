> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Project 1 Requirements:

*Three Parts:*

1. Clone Student Files
2. Edit .jsp pages
3. Chapter Questions (CHs 9-10)

#### README.md file should include the following items:

* Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos
you created in README.md, using Markdown syntax
(README.md must also include screenshots as per above.)
* Blackboard Links: lis4368 Bitbucket repo
* The carousel must contain (min. 3) slides that either contain text or images that link
to other content areas marketing/promoting your skills.



#### Assignment Screenshots:

*Screenshot of Main Page:

![Main Page](img/main_page.png)

*Screenshot of Failed Validation:

![Failed Validation](img/failed_validation.png)

*Screenshot of Passed Validation:

![Passed Validation](img/passed_validation.png)



