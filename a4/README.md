> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Assignment 4 Requirements:

*Three Parts:*

1. Create Java Servlet Pages
2. Compile the Java files
3. Chapter Questions (Ch 11-12)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of Passed Validation;
* Screenshot of Failed Validation;



#### Assignment Screenshots:

*Screenshot of query results:

![Passed Validation](img/passed.png)
![Failed Validation](img/failed.png)



