> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Project 2 Requirements:

*Three Parts:*

1. Create Data folder
2. Compile the Java files
3. Chapter Questions (Ch 16-17)

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry;
* Screenshot of Passed Validation;
* Screenshot of Display Data
* Screenshot of Modify Form
* Screenshot of Modified Data
* Screenshot of Delete Warning
* Screenshot of Associated Database Changes



#### Assignment Screenshots:

*Screenshots:

![Entry](img/1.png)
![Entry](img/2.png)
![Entry](img/3.png)
![Entry](img/4.png)
![Entry](img/5.png)
![Entry](img/6.png)
![Entry](img/7.png)



