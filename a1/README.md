> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Assignment 1 Requirements:

*Part 1 and 2:*

1. Install Git
2. Create bitbucket account
3. Set up SSH for Git
4. Install JDK
5. Install Tomcat
6. Clone assignments
7. Answer questions on blackboard

#### README.md file should include the following items:

* Screenshot of running java Hello;
* Screenshot of running Tomcat http://localhost:9999;
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes)


> #### Git commands w/short descriptions:

1. git init- create a new local repository
2. git add- add one or more files 
3. git commit- commit changes to head
4. git push origin master- send changes to the master branch of remote repository
5. git status- list files changed and that need to add or commit
6. git branch- list all branches in repo
7. git pull- get and merge changes on the remote server to working directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/francesallenn/bitbucketstationlocations/ "Bitbucket Station Locations")

