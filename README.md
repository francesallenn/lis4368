> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368 Advanced Web Applications Development

## Frances Allen

### Assignment # Requirements:

*Course Work Links:*

### [A1 README.md](a1/README.md "My A1 README.md file")

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Ch 1-4)

### [A2 README.md](a2/README.md "My A2 README.md file")

1. Download and install MySQL
2. Develop and Deploy a WebApp
3. Chapter Questions (Ch 5-6)

### [A3 README.md](a3/README.md "My A3 README.md file")

1. Create a mySQL database with three tables
2. Enter 10 inserts in each table
3. Chapter Questions (Ch 7-8)

### [P1 README.md](p1/README.md "My P1 README.md file")

1. Clone Student Files
2. Edit .jsp pages
3. Chapter Questions (CHs 9-10)

### [A4 README.md](a4/README.md "My A4 README.md file")

1. Create Java Servlet Pages
2. Compile the Java files
3. Chapter Questions (Ch 11-12)

### [A5 README.md](a5/README.md "My A5 README.md file")

1. Create Data folder
2. Compile the Java files
3. Chapter Questions (Ch 13-15)

### [P2 README.md](p2/README.md "My P2 README.md file")

1. Create Data folder
2. Compile the Java files
3. Chapter Questions (Ch 16-17)
