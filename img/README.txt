This image is for an example *only*, and *must be* changed.

NB: 10 PTS WILL BE TAKEN OFF FOR NOT CHANGING IT.

Use a photo of yourself, create your own photos/images, or find public domain or
freely licensed images.
