> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Assignment 2 Requirements:

*Part 1 and 2:*

1. Download and install MySQL
2. Develop and Deploy a WebApp
3. Chapter Questions (Ch 5-6)

#### README.md file should include the following items:

* Assessment links (as above), and
* Screenshot of the query results from the following link (see screenshot below):
http://localhost:9999/hello/querybook.html


####  Assessment: the following links should properly display (see screenshots below):


* [http://localhost:9999/hello](http://localhost:9999/hello) (displays directory, needs index.html)
* [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html) (Rename "HelloHome.html" to "index.html")
* [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello) (invokes HelloServlet)
* [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
* [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi) (invokes AnotherHelloServlet)


#### Assignment Screenshots:

*Screenshot of query results:

![Query Results](img/queryresults.png)



