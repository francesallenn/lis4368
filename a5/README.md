> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Application Development

## Frances Allen

### Assignment 5 Requirements:

*Three Parts:*

1. Create Data folder
2. Compile the Java files
3. Chapter Questions (Ch 13-15)

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry;
* Screenshot of Passed Validation;
* Associated Database Entry



#### Assignment Screenshots:

*Screenshots:

![Entry](img/before.png)
![Entry](img/after.png)
![Entry](img/mysql.png)



